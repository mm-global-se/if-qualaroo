mmcore.IntegrationFactory.register('Qualaroo', {
    defaults: {
        // read why here: https://bitbucket.org/mm-global-se/integration-factory#markdown-header-properties_1
        isStopOnDocEnd: false
    },

    validate: function (data) {
        if(!data.campaign)
            return 'No campaign.';

        return true;
    },

    check: function (data) {
        return window._kiq && typeof window._kiq.push;
    },

    exec: function (data) {
        var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
            info = mode + data.campaignInfo;

        _kiq.push(['set', {'MVT': info}]);

        if (typeof data.callback === 'function') data.callback.call(null, data.campaignInfo);
        return true;
    }
});