# Qualaroo

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __Qualaroo__.

__Note:__ Campaign generation info will be added once Qualaroo endpoint is available, but will be sent once Qualaroo survey form is submited!

## How we send the data

Qualaroo provides global object called `window._kiq`. We are using its `push` method to send campaign experience information (see example below).

_Example_:

```javascript
_kiq.push(['set', {'MVT': 'MM_Prod_T1Button=color:red'}]);
```

## Data Format

The data sent to Kissmetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [qualaroo-register.js](https://bitbucket.org/mm-global-se/if-qualaroo/src/master/src/qualaroo-register.js)

* [qualaroo-initialize.js](https://bitbucket.org/mm-global-se/if-qualaroo/src/master/src/qualaroo-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [qualaroo-register.js](https://bitbucket.org/mm-global-se/if-qualaroo/src/master/src/qualaroo-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [qualaroo-initialize.js](https://bitbucket.org/mm-global-se/if-qualaroo/src/master/src/qualaroo-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

+ Go to generation page

+ Wait for survey to appear

+ Fill the survey and submit the survey

+ Go to Network tab in Chrome DevTools

+ Filter by "kissinsights"

    ![q-0.png](https://bitbucket.org/repo/7oMX9a/images/2572360253-q-0.png)

+ Look for campaign generation info in "Query String Parameters" section

    ![q-1.png](https://bitbucket.org/repo/7oMX9a/images/4176119616-q-1.png)